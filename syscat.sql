---------------------------------------------------------------------------
--
-- syscat.sql-
--    Exemplos de consultas aos cat�logos do sistema
--
--
-- Portions Copyright (c) 1996-2003, PostgreSQL Global Development Group
-- Portions Copyright (c) 1994, Regents of the University of California
--
-- $Id: syscat.sql,v 1.2 2005/05/22 12:35:08 halleypo Exp $
--
---------------------------------------------------------------------------

--
-- Primeiro definir o caminho de procura do esquema como pg_catalog,
-- para n�o ser necess�rio qualificar todo objeto do sistema.
--
SET SEARCH_PATH TO pg_catalog;

--
-- Listar o nome de todos os administradores de banco de dados e o
-- nome de seus bancos de dados.
--
SELECT usename, datname
  FROM pg_user, pg_database
  WHERE usesysid = datdba
  ORDER BY usename, datname;

--
-- Listar todas as classes definidas pelo usu�rio
--
SELECT n.nspname, c.relname
  FROM pg_class c, pg_namespace n
  WHERE c.relnamespace=n.oid
    AND c.relkind = 'r'                   -- sem �ndices, vis�es, etc
    AND n.nspname not like 'pg\\_%'       -- sem cat�logos
    AND n.nspname != 'information_schema' -- sem information_schema
  ORDER BY nspname, relname;

--
-- Listar todos os �ndices simples (ou seja, �queles que s�o definidos
-- sobre uma refer�ncia de coluna simples)
--
SELECT n.nspname AS schema_name,
       bc.relname AS class_name, 
       ic.relname AS index_name, 
       a.attname
  FROM pg_namespace n,
       pg_class bc,             -- classe base
       pg_class ic,             -- classe �ndice
       pg_index i,
       pg_attribute a           -- atributo na base
  WHERE bc.relnamespace = n.oid
    AND i.indrelid = bc.oid
    AND i.indexrelid = ic.oid
    AND i.indkey[0] = a.attnum
    AND i.indnatts = 1
    AND a.attrelid = bc.oid
  ORDER BY schema_name, class_name, index_name, attname;

--
-- Listar os atributos definidos pelo usu�rio e seus tipos
-- para todas as classes definidas pelo usu�rio
--
SELECT n.nspname, c.relname, a.attname, format_type(t.oid, null) AS typname
  FROM pg_namespace n, pg_class c, 
       pg_attribute a, pg_type t
  WHERE n.oid = c.relnamespace
    AND c.relkind = 'r'                   -- sem �ndices
    AND n.nspname not like 'pg\\_%'       -- sem cat�logos
    AND n.nspname != 'information_schema' -- sem information_schema
    AND a.attnum > 0                      -- sem atributo de sistema
    AND not a.attisdropped                -- sem colunas removidas
    AND a.attrelid = c.oid
    AND a.atttypid = t.oid
  ORDER BY nspname, relname, attname;

--
-- Listar todos os tipos base definidos pelo usu�rio (sem incluir os tipos matriz)
--
SELECT n.nspname, u.usename, format_type(t.oid, null) AS typname
  FROM pg_type t, pg_user u, pg_namespace n
  WHERE u.usesysid = t.typowner
    AND t.typnamespace = n.oid
    AND t.typrelid = '0'::oid             -- sem tipos complexos
    AND t.typelem = '0'::oid              -- sem matrizes
    AND n.nspname not like 'pg\\_%'       -- sem cat�logos
    AND n.nspname != 'information_schema' -- sem information_schema
  ORDER BY nspname, usename, typname;

--
-- Listar todos os operadores un�rios esquerdos
--
SELECT n.nspname, o.oprname AS left_unary, 
       format_type(right_type.oid, null) AS operand,
       format_type(result.oid, null) AS return_type
  FROM pg_namespace n, pg_operator o, 
       pg_type right_type, pg_type result
  WHERE o.oprnamespace = n.oid
    AND o.oprkind = 'l'           -- un�rio esquerdo
    AND o.oprright = right_type.oid
    AND o.oprresult = result.oid
  ORDER BY nspname, operand;

--
-- Listar todos os operadores un�rios direitos
--
SELECT n.nspname, o.oprname AS right_unary, 
       format_type(left_type.oid, null) AS operand,
       format_type(result.oid, null) AS return_type
  FROM pg_namespace n, pg_operator o, 
       pg_type left_type, pg_type result
  WHERE o.oprnamespace = n.oid
    AND o.oprkind = 'r'          -- un�rio direito
    AND o.oprleft = left_type.oid
    AND o.oprresult = result.oid
  ORDER BY nspname, operand;

--
-- Listar todos os operadores bin�rios
--
SELECT n.nspname, o.oprname AS binary_op,
       format_type(left_type.oid, null) AS left_opr,
       format_type(right_type.oid, null) AS right_opr,
       format_type(result.oid, null) AS return_type
  FROM pg_namespace n, pg_operator o, pg_type left_type, 
       pg_type right_type, pg_type result
  WHERE o.oprnamespace = n.oid
    AND o.oprkind = 'b'         -- bin�rio
    AND o.oprleft = left_type.oid
    AND o.oprright = right_type.oid
    AND o.oprresult = result.oid
  ORDER BY nspname, left_opr, right_opr;

--
-- Listar o nome, n�mero de argumentos e o tipo retornado por
-- todas as fun��es C definidas pelo usu�rio
--
SELECT n.nspname, p.proname, p.pronargs, format_type(t.oid, null) AS return_type
  FROM pg_namespace n, pg_proc p, 
       pg_language l, pg_type t
  WHERE p.pronamespace = n.oid
    AND n.nspname not like 'pg\\_%'       -- sem cat�logos
    AND n.nspname != 'information_schema' -- sem information_schema
    AND p.prolang = l.oid 
    AND p.prorettype = t.oid
    AND l.lanname = 'c'
  ORDER BY nspname, proname, pronargs, return_type;

--
-- Listar todas as fun��es de agrega��o e os tipos em que podem ser aplicadas
--
SELECT n.nspname, p.proname, format_type(t.oid, null) AS typname
  FROM pg_namespace n, pg_aggregate a, 
       pg_proc p, pg_type t
  WHERE p.pronamespace = n.oid
    AND a.aggfnoid = p.oid
    AND p.proargtypes[0] = t.oid
  ORDER BY nspname, proname, typname;

--
-- Lista todas as classes de operadores que podem ser utilizadas com
-- cada m�todo de acesso, assim como os operadores que podem ser usados
-- com as respectivas classes de operadores
--
SELECT n.nspname, am.amname, opc.opcname, opr.oprname
  FROM pg_namespace n, pg_am am, pg_opclass opc, 
       pg_amop amop, pg_operator opr
  WHERE opc.opcnamespace = n.oid
    AND opc.opcamid = am.oid
    AND amop.amopclaid = opc.oid
    AND amop.amopopr = opr.oid
  ORDER BY nspname, amname, opcname, oprname;

--
-- Restaurar o caminho de procura
--
RESET SEARCH_PATH;
