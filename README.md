### Introdução

Tradução da documentação do PostgreSQL 8.0 para o Português do Brasil,
permitindo a todos os interessados consultar, baixar, colocar em seus
próprios sites, distribuir através de CDs, ou de qualquer outra mídia,
esta documentação traduzida.


### Sítios na Web

#### [Documentação do PostgreSQL 8.0](http://pgdocptbr.sourceforge.net/pg80/index.html)
    
#### [Documentação do PostgreSQL 14.4](https://pgdocptbr.xyz)
