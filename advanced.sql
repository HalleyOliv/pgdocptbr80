---------------------------------------------------------------------------
--
-- advanced.sql-
--    Tutorial sobre funcionalidades avan�adas do PostgreSQL
--
--
-- Copyright (c) 1994, Regents of the University of California
--
-- $Id: advanced.sql,v 1.2 2005/06/10 14:29:50 halleypo Exp $
--
---------------------------------------------------------------------------

-----------------------------
-- Heran�a:
--      Uma tabela pode herdar de zero ou mais tabelas. Uma consulta pode
--      fazer refer�ncia a todas as linhas da tabela, ou a todas as linhas
--      da tabela mais todas as linhas de suas tabelas descendentes.
-----------------------------

-- Por exemplo, a tabela capitais herda da tabela cidades table (Herda 
-- todos os campos de dado de cidades).

CREATE TABLE cidades (
        nome            text,
        populacao       float8,
        altitude        int         -- (em p�s)
);

CREATE TABLE capitais (
        estado          char(2)
) INHERITS (cidades);

-- Agora ser�o carregados dados nas tabelas.
INSERT INTO cidades VALUES ('S�o Francisco', 7.24E+5, 63);
INSERT INTO cidades VALUES ('Las Vegas', 2.583E+5, 2174);
INSERT INTO cidades VALUES ('Mariposa', 1200, 1953);

INSERT INTO capitais VALUES ('Sacramento', 3.694E+5, 30, 'CA');
INSERT INTO capitais VALUES ('Madison', 1.913E+5, 845, 'WI');

SELECT * FROM cidades;
SELECT * FROM capitais;

-- Podem ser encontradas todas as cidades, incluindo as capitais,
-- localizadas a uma altitude de 500 p�s ou mais usando:

SELECT c.nome, c.altitude
    FROM  cidades c
    WHERE c.altitude > 500;

-- Para varrer apenas a tabela ancestral � usado ONLY:

SELECT nome, altitude
    FROM ONLY cidades
    WHERE altitude > 500;

-- limpeza (a tabela descendente deve ser removida primeiro)
DROP TABLE capitais;
DROP TABLE cidades;
