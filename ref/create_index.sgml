<!--
$Header: /cvsroot/pgdocptbr/pgsgml800/doc/src/sgml/ref/create_index.sgml,v 1.14 2007/02/15 21:57:30 halleypo Exp $
PostgreSQL documentation
-->

<refentry id="SQL-CREATEINDEX">
 <refmeta>
  <refentrytitle id="sql-createindex-title">CREATE INDEX</refentrytitle>
  <refmiscinfo>SQL - Comandos da Linguagem</refmiscinfo>
 </refmeta>

 <refnamediv>
  <refname>CREATE INDEX</refname>
  <refpurpose>cria um �ndice</refpurpose>
 </refnamediv>

 <indexterm zone="sql-createindex">
  <primary>CREATE INDEX</primary>
 </indexterm>

 <refsynopsisdiv>
<synopsis>
CREATE [ UNIQUE ] INDEX <replaceable class="parameter">nome_do_�ndice</replaceable> ON <replaceable class="parameter">tabela</replaceable> [ USING <replaceable class="parameter">m�todo</replaceable> ]
    ( { <replaceable class="parameter">coluna</replaceable> | ( <replaceable class="parameter">express�o</replaceable> ) } [ <replaceable class="parameter">classe_de_operadores</replaceable> ] [, ...] )
    [ TABLESPACE <replaceable class="parameter">espa�o_de_tabelas</replaceable> ]
    [ WHERE <replaceable class="parameter">predicado</replaceable> ]
</synopsis>
 </refsynopsisdiv>

 <refsect1>
  <title>Descri��o</title>

  <para>
   O comando <command>CREATE INDEX</command> constr�i o �ndice
   <replaceable class="parameter">nome_do_�ndice</replaceable> na tabela
   especificada. Os �ndices s�o utilizados, principalmente, para melhorar o
   desempenho do banco de dados (embora a utiliza��o n�o apropriada possa
   resultar em uma degrada��o de desempenho).
   <footnote>
    <para>
     <productname>Oracle</productname> &mdash;
     O comando <command>CREATE INDEX</command> � utilizado para criar um �ndice
     em:
     1) Uma ou mais colunas de uma tabela, de uma tabela particionada, de uma
     tabela organizada pelo �ndice, de um agrupamento
     (<foreignphrase>cluster</> = objeto de esquema que cont�m dados de uma
     ou mais tabelas, todas tendo uma ou mais colunas em comum.
     O banco de dados <productname>Oracle</productname> armazena todas as
     linhas de todas as tabelas que compartilham a mesma chave de agrupamento
     juntas);
     2) Um ou mais atributos de objeto tipado escalar de uma tabela ou de um
     agrupamento;
     3) Uma tabela de armazenamento de tabela aninhada para indexar uma coluna
     de tabela aninhada.
     <ulink url="http://download-east.oracle.com/docs/cd/B14117_01/server.101/b10759/statements_5010.htm">
     <trademark class='registered'>Oracle</trademark> Database SQL Reference
     10g Release 1 (10.1) Part Number B10759-01</ulink> (N. do T.)
    </para>
   </footnote>
   <footnote>
    <para>
     <productname>Oracle</productname> &mdash;
     A <emphasis>tabela organizada pelo �ndice</emphasis> � um tipo especial
     de tabela que armazena as linhas da tabela dentro de segmento de �ndice.
     A tabela organizada pelo �ndice tamb�m pode ter um segmento de estouro
     (<foreignphrase>overflow</>) para armazenar as linhas que n�o cabem
     no segmento de �ndice original.
     <ulink url="http://www.mhprofessional.com/product.php?cat=112&amp;isbn=007226327X">
     Hands-On Oracle Database 10g Express Edition for Linux &mdash;
     Steve Bobrowski</ulink>, p�g. 350 (N. do T.)
    </para>
   </footnote>
   <footnote>
    <para>
     <productname>SQL Server</productname> &mdash;
     O comando <command>CREATE INDEX</command> cria um �ndice relacional em uma
     tabela ou vis�o especificada, ou um �ndice XML em uma tabela especificada.
     O �ndice pode  ser criado antes de existir dado na tabela.
     Podem ser criados �ndices para tabelas e vis�es em outros bancos de dados
     especificando um nome de banco de dados qualificado.
     O argumento <literal>CLUSTERED</literal> cria um �ndice em que a ordem
     l�gica dos valores da chave determina a ordem f�sica das linhas
     correspondentes da tabela.
     A cria��o de um �ndice agrupado (<foreignphrase>clustered</>) �nico em
     uma vis�o melhora o desempenho, porque a vis�o � armazenada no banco de
     dados da mesma maneira que a tabela com um �ndice agrupado � armazenada.
     Podem ser criados �ndices em colunas calculadas.
     No <productname>SQL Server 2005</productname> as colunas calculadas podem
     ter a propriedade <property>PERSISTED</property>.
     Isto significa que o Mecanismo de Banco de Dados armazena os valores
     calculados, e os atualiza quando as outras colunas das quais a coluna
     calculada depende s�o atualizadas.
     O Mecanismo de Banco de Dados utiliza os valores persistentes quando cria
     o �ndice para a coluna, e quando o �ndice � referenciado em uma consulta.
     <ulink url="http://msdn2.microsoft.com/en-us/library/ms188783.aspx">
     SQL Server 2005 Books Online &mdash; CREATE INDEX (Transact-SQL)</ulink>
     (N. do T.)
    </para>
   </footnote>
   <footnote>
    <para>
     <productname>DB2</productname> &mdash;
     O comando <command>CREATE INDEX</command> � utilizado para:
     Definir um �ndice em uma tabela do <productname>DB2</productname>
     (o �ndice pode ser definido em dados XML ou dados relacionais);
     Criar uma especifica��o de �ndice (metadados que indicam ao otimizador
     que a tabela de origem possui um �ndice).
     A cl�usula <literal>CLUSTER</literal> especifica que o �ndice � o �ndice
     agrupador da tabela.
     O fator de agrupamento de um �ndice agrupador � mantido ou melhorado
     dinamicamente � medida que os dados s�o inseridos na tabela associada,
     tentando inserir as novas linhas fisicamente pr�ximas das linhas para as
     quais os valores chave deste �ndice est�o no mesmo intervalo.
     <ulink url="http://publib.boulder.ibm.com/infocenter/db2luw/v9/topic/com.ibm.db2.udb.admin.doc/doc/r0000919.htm">
     DB2 Version 9 for Linux, UNIX, and Windows</ulink> (N. do T.)
    </para>
   </footnote>
  </para>

  <para>
   Os campos chave para o �ndice s�o especificados como nomes de coluna ou,
   tamb�m, como express�es escritas entre par�nteses.
   Podem ser especificados v�rios campos, se o m�todo de �ndice suportar
   �ndices multicolunas.
  </para>

  <para>
   O campo de um �ndice pode ser uma express�o computada a partir dos valores de
   uma ou mais colunas da linha da tabela. Esta funcionalidade pode ser
   utilizada para obter acesso r�pido aos dados baseado em alguma transforma��o
   dos dados b�sicos. Por exemplo, um �ndice computado como
   <function>upper(col)</function> permite a cl�usula
   <literal>WHERE upper(col) = 'JIM'</literal> utilizar um �ndice.
  </para>

  <para>
   O <productname>PostgreSQL</productname> fornece os m�todos de �ndice
   <systemitem>B-tree</>, <systemitem>R-tree</>, <systemitem>hash</>
   e <systemitem>GiST</>. O m�todo de �ndice <systemitem>B-tree</> � uma
   implementa��o das <foreignphrase>B-trees</> de alta concorr�ncia de
   Lehman-Yao
   <footnote>
    <para>
     Lehman, Yao 81 - Philip L. Lehman , s. Bing Yao, Efficient locking for
     concurrent operations on B-trees, ACM Transactions on Database Systems
     (TODS), v.6 n.4, p.650-670, Dec. 1981 (N. do T.)
    </para>
   </footnote>
   . O m�todo de �ndice <systemitem>R-tree</> implementa
   <literal>R-trees</literal> padr�o utilizando o algoritmo de divis�o
   quadr�tica (<foreignphrase>quadratic split</foreignphrase>) de Guttman
   <footnote>
    <para>
     Antonin Guttman: R-Trees: A Dynamic Index Structure for Spatial Searching.
     SIGMOD Conference 1984. (N. do T.)
    </para>
   </footnote>
   . O m�todo de �ndice <systemitem>hash</> � uma
   implementa��o do <systemitem>hash</> linear de Litwin
   <footnote>
    <para>
     Litwin, W. Linear hashing: A new tool for file and table addressing.
     In Proceedings of the 6th Conference on Very Large Databases,
     (New York, 1980}, 212-223. (N. do T.)
    </para>
   </footnote>
   <footnote>
    <para>
     Witold Litwin:
     <ulink url="http://swig.stanford.edu/pub/summaries/database/linhash.html">
     Linear Hashing: A new Tool for File and Table Addressing</ulink> -
     Summary by: Steve Gribble and Armando Fox. (N. do T.)
    </para>
   </footnote>
   . Os usu�rios tamb�m podem definir seus pr�prios m�todos de �ndice,
   mas � muito complicado.
  </para>

  <para>
    Quando a cl�usula <literal>WHERE</literal> est� presente, � criado um
    <firstterm>�ndice parcial</firstterm>. Um �ndice parcial � um �ndice
    contendo entradas para apenas uma parte da tabela, geralmente uma parte
    mais �til para indexar do que o restante da tabela. Por exemplo, havendo
    uma tabela contendo tanto pedidos faturados quanto n�o faturados, onde os
    pedidos n�o faturados ocupam uma pequena parte da tabela, mas que �
    bastante usada, o desempenho pode ser melhorado criando um �ndice apenas
    para esta parte da tabela. Outra aplica��o poss�vel � utilizar a cl�usula
    <literal>WHERE</literal> junto com <literal>UNIQUE</literal> para impor a
    unicidade de um subconjunto dos dados da tabela.
    Para obter informa��es adicionais deve ser consultada a
    <xref linkend="indexes-partial"> .
  </para>

  <para>
    A express�o utilizada na cl�usula <literal>WHERE</literal> pode referenciar
    apenas as colunas da tabela subjacente, mas pode usar todas as colunas,
    e n�o apenas as que est�o sendo indexadas. Atualmente n�o s�o permitidas
    subconsultas e express�es de agrega��o na cl�usula <literal>WHERE</literal>.
    As mesmas restri��es se aplicam aos campos do �ndice que s�o express�es.
  </para>

  <para>
   Todas as fun��es e operadores utilizados na defini��o do �ndice devem ser
   <quote>imut�veis</quote> (<foreignphrase>immutable</>), ou seja, seus
   resultados devem depender somente de seus argumentos, e nunca de uma
   influ�ncia externa (como o conte�do de outra tabela ou a hora atual). Esta
   restri��o garante que o comportamento do �ndice � bem definido. Para utilizar
   uma fun��o definida pelo usu�rio na express�o do �ndice ou na cl�usula
   <literal>WHERE</literal>, a fun��o deve ser marcada como
   <literal>IMMUTABLE</literal> na sua cria��o.
  </para>
 </refsect1>

 <refsect1>
  <title>Par�metros</title>

    <variablelist>
     <varlistentry>
      <term><literal>UNIQUE</literal></term>
      <listitem>
       <para>
        Faz o sistema verificar valores duplicados na tabela quando o �ndice �
        criado, se existirem dados, e toda vez que forem adicionados dados. A
        tentativa de inserir ou de atualizar dados que resultem em uma entrada
        duplicada gera um erro.
       </para>
      </listitem>
     </varlistentry>

     <varlistentry>
      <term><replaceable class="parameter">nome_do_�ndice</replaceable></term>
      <listitem>
       <para>
        O nome do �ndice a ser criado. O nome do esquema n�o pode ser inclu�do
        aqui; o �ndice � sempre criado no mesmo esquema da tabela que este
        pertence.
       </para>
      </listitem>
     </varlistentry>

     <varlistentry>
      <term><replaceable class="parameter">tabela</replaceable></term>
      <listitem>
       <para>
        O nome (opcionalmente qualificado pelo esquema) da tabela a ser indexada.
       </para>
      </listitem>
     </varlistentry>

     <varlistentry>
      <term><replaceable class="parameter">m�todo</replaceable></term>
      <listitem>
       <para>
        O nome do m�todo de �ndice a ser utilizado. Pode ser escolhido entre
        <systemitem>btree</systemitem>, <systemitem>hash</systemitem>,
        <systemitem>rtree</systemitem> e <systemitem>gist</systemitem>. O m�todo
        padr�o � <systemitem>btree</systemitem>.
       </para>
      </listitem>
     </varlistentry>

     <varlistentry>
      <term><replaceable class="parameter">coluna</replaceable></term>
      <listitem>
       <para>
        O nome de uma coluna da tabela.
       </para>
      </listitem>
     </varlistentry>

     <varlistentry>
      <term><replaceable class="parameter">express�o</replaceable></term>
      <listitem>
       <para>
        Uma express�o baseada em uma ou mais colunas da tabela.
        Geralmente a express�o deve ser escrita entre par�nteses,
        conforme mostrado na sintaxe. Entretanto, os par�nteses podem ser
        omitidos se a express�o tiver a forma de uma chamada de fun��o.
       </para>
      </listitem>
     </varlistentry>

     <varlistentry>
      <term><replaceable class="parameter">classe_de_operadores</replaceable></term>
      <listitem>
       <para>
        O nome de uma classe de operadores. Veja os detalhes abaixo.
       </para>
      </listitem>
     </varlistentry>

     <varlistentry>
      <term><replaceable class="parameter">espa�o_de_tabelas</replaceable></term>
      <listitem>
       <para>
        O espa�o de tabelas onde o �ndice ser� criado. Se n�o for especificado,
        ser� utilizado o <xref linkend="guc-default-tablespace">, ou o espa�o de
        tabelas padr�o do banco de dados se <varname>default_tablespace</>
        for uma cadeia de caracteres vazia.
       </para>
      </listitem>
     </varlistentry>

     <varlistentry>
      <term><replaceable class="parameter">predicado</replaceable></term>
      <listitem>
       <para>
        A express�o de restri��o para o �ndice parcial.
       </para>
      </listitem>
     </varlistentry>

    </variablelist>
 </refsect1>

 <refsect1>
  <title>Observa��es</title>

  <para>
   Consulte o <xref linkend="indexes"> para obter informa��es sobre quando os
   �ndices podem ser utilizados, quando n�o s�o utilizados, e em quais situa��es
   particulares podem ser �teis.
  </para>

  <para>
   Atualmente somente os m�todos de �ndice <systemitem>B-tree</systemitem> e
   <systemitem>GiST</> suportam �ndices com mais de uma coluna. Por padr�o
   podem ser especificadas at� 32 colunas (este limite pode ser alterado na
   constru��o do <productname>PostgreSQL</productname>). Tamb�m atualmente
   somente <systemitem>B-tree</systemitem> suporta �ndices �nicos.
  </para>

  <para>
   Pode ser especificada uma <firstterm>classe de operadores</firstterm> para
   cada coluna de um �ndice. A classe de operadores identifica os operadores a
   serem utilizados pelo �ndice para esta coluna. Por exemplo, um �ndice
   <systemitem>B-tree</> sobre inteiros de quatro bytes usaria a classe
   <literal>int4_ops</literal>; esta classe de operadores inclui fun��es de
   compara��o para inteiros de quatro bytes. Na pr�tica, a classe de operadores
   padr�o para o tipo de dado da coluna � normalmente suficiente. O ponto
   principal em haver classes de operadores � que, para alguns tipos de dado,
   pode haver mais de uma ordena��o que fa�a sentido. Por exemplo, pode-se
   desejar classificar o tipo de dado do n�mero complexo tanto pelo valor
   absoluto quanto pela parte real, o que pode ser feito definindo duas classes
   de operadores para o tipo de dado e, ent�o, selecionando a classe apropriada
   na constru��o do �ndice. Mais informa��es sobre classes de operadores est�o
   na <xref linkend="indexes-opclass"> e na <xref linkend="xindex">.
  </para>

  <para>
   Para remover um �ndice deve ser utilizado o comando
   <xref linkend="sql-dropindex" endterm="sql-dropindex-title">.
  </para>

  <para>
   Por padr�o n�o � utilizado �ndice para a cl�usula <literal>IS NULL</>.
   A melhor forma para utilizar �ndice nestes casos � a cria��o de um �ndice
   parcial usando o predicado <literal>IS NULL</>.
  </para>
 </refsect1>

 <refsect1>
  <title>Exemplos</title>

  <para>
   Para criar um �ndice <systemitem>B-tree</systemitem> para a coluna
   <database class="field">titulo</database> na tabela
   <database class="table">filmes</database>:
<programlisting>
CREATE UNIQUE INDEX unq_titulo ON filmes (titulo);
</programlisting>
  </para>

  <para>
   Para criar um �ndice para a coluna <database class="field">codigo</>
   da tabela <database class="table">filmes</> e fazer o �ndice residir no
   espa�o de tabelas <database>espaco_indices</database>:
<programlisting>
CREATE INDEX idx_codigo ON filmes(codigo) TABLESPACE espaco_indices;
</programlisting>
  </para>

<!--
<comment>
Is this example correct?
</comment>
  <para>
   To create a R-tree index on a point attribute so that we
   can efficiently use box operators on the result of the
   conversion function:
  </para>
<programlisting>
CREATE INDEX pointloc
    ON points USING RTREE (point2box(location) box_ops);
SELECT * FROM points
    WHERE point2box(points.pointloc) = boxes.box;
</programlisting>
-->

 </refsect1>

 <refsect1>
  <title>Compatibilidade</title>

  <para>
   O comando <command>CREATE INDEX</command> � uma extens�o do
   <productname>PostgreSQL</productname> � linguagem.
   O padr�o SQL n�o trata de �ndices.
  </para>
 </refsect1>

 <refsect1>
  <title>Consulte tamb�m</title>

  <simplelist type="inline">
   <member><xref linkend="sql-alterindex" endterm="sql-alterindex-title"></member>
   <member><xref linkend="sql-dropindex" endterm="sql-dropindex-title"></member>
  </simplelist>
 </refsect1>
</refentry>

<!-- Keep this comment at the end of the file
Local variables:
mode: sgml
sgml-omittag:nil
sgml-shorttag:t
sgml-minimize-attributes:nil
sgml-always-quote-attributes:t
sgml-indent-step:1
sgml-indent-data:t
sgml-parent-document:nil
sgml-default-dtd-file:"../reference.ced"
sgml-exposed-tags:nil
sgml-local-catalogs:"/usr/lib/sgml/catalog"
sgml-local-ecat-files:nil
End:
-->
