<!-- $Header: /cvsroot/pgdocptbr/pgsgml800/doc/src/sgml/ref/set_constraints.sgml,v 1.7 2007/02/15 21:57:31 halleypo Exp $ -->
<refentry id="SQL-SET-CONSTRAINTS">
 <refmeta>
  <refentrytitle id="SQL-SET-CONSTRAINTS-title">SET CONSTRAINTS</refentrytitle>
  <refmiscinfo>SQL - Comandos da Linguagem</refmiscinfo>
 </refmeta>

 <refnamediv>
  <refname>SET CONSTRAINTS</refname>
  <refpurpose>define os modos de verifica��o da restri��o na transa��o corrente</refpurpose>
 </refnamediv>

 <indexterm zone="sql-set-constraints">
  <primary>SET CONSTRAINTS</primary>
 </indexterm>

 <refsynopsisdiv>
<synopsis>
SET CONSTRAINTS { ALL | <replaceable class="parameter">nome</replaceable> [, ...] } { DEFERRED | IMMEDIATE }
</synopsis>
 </refsynopsisdiv>

 <refsect1>
  <title>Descri��o</title>

  <para>
   O comando <command>SET CONSTRAINTS</command> define o comportamento da
   verifica��o da restri��o dentro da transa��o corrente.
   No modo <literal>IMMEDIATE</literal> (imediato), as restri��es s�o
   verificadas ao final de cada comando.
   No modo <literal>DEFERRED</> (postergado), as restri��es n�o s�o verificadas
   at� o momento da efetiva��o da transa��o (<foreignphrase>commit</>).
   Cada restri��o possui seu pr�prio modo <literal>IMMEDIATE</literal> ou
   <literal>DEFERRED</literal>.
   <footnote>
    <para>
    4.7.1 &mdash; <emphasis>Verifica��o da restri��o</emphasis> &mdash;
    Existem dois tipos de objeto do esquema que descrevem restri��es:
    asser��es e restri��es de tabela (incluindo as restri��es de dom�nio de
    qualquer dom�nio sobre o qual uma coluna da tabela pode estar baseada),
    e s�o verificados da mesma maneira.
    Toda restri��o � <emphasis>posterg�vel</emphasis> ou n�o
    <emphasis>posterg�vel</emphasis>.
    Em toda sess�o <acronym>SQL</acronym>, toda restri��o possui um modo da
    restri��o que � uma propriedade desta sess�o <acronym>SQL</acronym>.
    Cada restri��o possui um modo da restri��o padr�o (persistente), com o
    qual a restri��o come�a cada transa��o <acronym>SQL</acronym> de cada
    sess�o <acronym>SQL</acronym>.
    O modo da restri��o � postergado ou imediato, e pode ser definido por um
    comando <acronym>SQL</acronym>, desde que a restri��o seja posterg�vel.
    Quando a transa��o � iniciada, o modo da restri��o de cada restri��o �
    definido como o seu modo padr�o.
    Ao completar a execu��o de todo comando <acronym>SQL</acronym>, toda
    restri��o cujo modo � imediato � verificada.
    Antes de terminar a transa��o, todo modo da restri��o � definido como
    imediato (e, portanto, verificado).
    (ISO-ANSI Working Draft) Framework (SQL/Framework), August 2003,
    ISO/IEC JTC 1/SC 32, 25-jul-2003, ISO/IEC 9075-1:2003 (E) (N. do T.)
    </para>
   </footnote>
   <footnote>
    <para>
     <productname>Oracle</productname> &mdash;
      O comando <command>SET CONSTRAINTS</command> � utilizado para especificar,
      para uma transa��o em particular, se a restri��o posterg�vel ser�
      verificada ap�s cada comando da <acronym>DML</> ou quando a transa��o for
      efetivada.
      Pode ser verificado se as restri��es posterg�veis ser�o bem-sucedidas
      antes de efetiv�-las submetendo o comando
      <command>SET CONSTRAINTS ALL IMMEDIATE</command>.
      <ulink url="http://download-east.oracle.com/docs/cd/B14117_01/server.101/b10759/statements_10003.htm">
      <trademark class='registered'>Oracle</trademark> Database SQL Reference
      10g Release 1 (10.1) Part Number B10759-01</ulink> (N. do T.)
    </para>
   </footnote>
  </para>

  <para>
   Ao ser criada, a restri��o sempre recebe uma destas tr�s caracter�sticas:
   <literal>DEFERRABLE INITIALLY DEFERRED</literal>
   (posterg�vel, inicialmente postergada),
   <literal>DEFERRABLE INITIALLY IMMEDIATE</literal>
   (posterg�vel, inicialmente imediata), ou
   <literal>NOT DEFERRABLE</literal> (n�o posterg�vel).
   A terceira classe � sempre <literal>IMMEDIATE</literal> (imediata) e n�o �
   afetada pelo comando <command>SET CONSTRAINTS</command>.
   As duas primeiras classes come�am todas as transa��es no modo indicado, mas
   seus comportamentos podem ser modificados dentro da transa��o pelo comando
   <command>SET CONSTRAINTS</command>.
  </para>

  <para>
   O comando <command>SET CONSTRAINTS</command> com uma lista de nomes de
   restri��o muda o modo destas restri��es apenas (que devem ser todos
   posterg�veis).
   Se existirem v�rias restri��es correspondendo a um nome fornecido, todos eles
   s�o afetados.
   O comando <command>SET CONSTRAINTS ALL</command> muda o modo de todas as
   restri��es posterg�veis.
  </para>

  <para>
   Quando o comando <command>SET CONSTRAINTS</command> muda o modo da restri��o
   de <literal>DEFERRED</literal> para <literal>IMMEDIATE</literal>, o novo
   modo passa a valer retroativamente: toda modifica��o de dados remanescente,
   que deveria ter sido verificada no final da transa��o, ser� verificada
   durante a execu��o do comando <command>SET CONSTRAINTS</command>.
   Se alguma destas restri��es estiver violada, o comando
   <command>SET CONSTRAINTS</> falhar� (e n�o mudar� o modo da restri��o).
   Portanto, o comando <command>SET CONSTRAINTS</command> pode ser utilizado
   para obrigar que ocorra a verifica��o das restri��es em um determinado ponto
   da transa��o.
  </para>

  <para>
   Atualmente, somente as restri��es de chave estrangeira s�o afetadas
   por esta defini��o.
   As restri��es de verifica��o (<foreignphrase>check</foreignphrase>)
   e de unicidade s�o sempre n�o posterg�veis.
  </para>
 </refsect1>

 <refsect1>
  <title>Observa��es</title>

  <para>
   Este comando somente altera o comportamento das restri��es dentro da
   transa��o corrente. Portanto, se este comando for executado fora
   de um bloco de transa��o
   (par <command>BEGIN</command>/<command>COMMIT</command>), parecer�
   que n�o produziu nenhum efeito.
  </para>
 </refsect1>

 <refsect1>
  <title>Compatibilidade</title>

  <para>
   Este comando est� em conformidade com o comportamento definido no padr�o
   <acronym>SQL</acronym>, exceto pela limita��o que, no
   <productname>PostgreSQL</productname>, somente se aplica �s restri��es de
   chave estrangeira.
  </para>

  <para>
   O padr�o SQL diz que os nomes das restri��es que aparecem no comando
   <command>SET CONSTRAINTS</command> podem ser qualificados pelo esquema.
   Esta funcionalidade ainda n�o � suportada pelo
   <productname>PostgreSQL</productname>: os nomes n�o podem ser qualificados,
   e todas as restri��es correspondendo ao comando ser�o afetadas,
   n�o importando o esquema em que estejam.
  </para>
 </refsect1>
</refentry>

<!-- Keep this comment at the end of the file
Local variables:
mode:sgml
sgml-omittag:nil
sgml-shorttag:t
sgml-minimize-attributes:nil
sgml-always-quote-attributes:t
sgml-indent-step:1
sgml-indent-data:t
sgml-parent-document:nil
sgml-default-dtd-file:"../reference.ced"
sgml-exposed-tags:nil
sgml-local-catalogs:("/usr/lib/sgml/catalog")
sgml-local-ecat-files:nil
End:
-->
