<!--
$Header: /cvsroot/pgdocptbr/pgsgml800/doc/src/sgml/ref/abort.sgml,v 1.5 2006/12/17 16:52:43 halleypo Exp $
PostgreSQL documentation
-->

<refentry id="SQL-ABORT">
 <refmeta>
  <refentrytitle id="sql-abort-title">ABORT</refentrytitle>
  <refmiscinfo>SQL - Comandos da Linguagem</refmiscinfo>
 </refmeta>

 <refnamediv>
  <refname>ABORT</refname>
  <refpurpose>interrompe a transa��o corrente</refpurpose>
 </refnamediv>

 <indexterm zone="sql-abort">
  <primary>ABORT</primary>
 </indexterm>

 <refsynopsisdiv>
<synopsis>
ABORT [ WORK | TRANSACTION ]
</synopsis>
 </refsynopsisdiv>

 <refsect1>
  <title>Descri��o</title>

  <para>
   O comando <command>ABORT</command> interrompe a transa��o corrente,
   fazendo com que todas as altera��es realizadas pela transa��o
   sejam desfeitas. Este comando possui comportamento id�ntico
   ao do comando do padr�o <acronym>SQL</acronym>
   <xref linkend="SQL-ROLLBACK" endterm="SQL-ROLLBACK-TITLE">,
   estando presente apenas por motivos hist�ricos.
  </para>
 </refsect1>

 <refsect1>
  <title>Par�metros</title>

  <variablelist>
   <varlistentry>
    <term><literal>WORK</literal></term>
    <term><literal>TRANSACTION</literal></term>
    <listitem>
     <para>
      Palavras chave opcionais. N�o produzem nenhum efeito.
     </para>
    </listitem>
   </varlistentry>
  </variablelist>
 </refsect1>

 <refsect1>
  <title>Observa��es</title>

  <para>
   Use o comando <xref linkend="SQL-COMMIT" endterm="SQL-COMMIT-TITLE">
   para terminar uma transa��o bem-sucedida.
  </para>

  <para>
   A utiliza��o do <command>ABORT</command> fora de uma transa��o n�o causa
   nenhum problema, mas provoca uma mensagem de advert�ncia.
  </para>
 </refsect1>

 <refsect1>
  <title>Exemplos</title>

  <para>
   Para interromper todas as altera��es:
<programlisting>
ABORT;
</programlisting>
  </para>
 </refsect1>

 <refsect1>
  <title>Compatibilidade</title>

  <para>
   Este comando � uma extens�o do <productname>PostgreSQL</productname> presente
   por motivos hist�ricos. O <command>ROLLBACK</command> � o comando
   equivalente do padr�o SQL.
  </para>
 </refsect1>

 <refsect1>
  <title>Consulte tamb�m</title>

  <simplelist type="inline">
   <member><xref linkend="sql-begin" endterm="sql-begin-title"></member>
   <member><xref linkend="sql-commit" endterm="sql-commit-title"></member>
   <member><xref linkend="sql-rollback" endterm="sql-rollback-title"></member>
  </simplelist>
 </refsect1>
</refentry>

<!-- Keep this comment at the end of the file
Local variables:
mode: sgml
sgml-omittag:nil
sgml-shorttag:t
sgml-minimize-attributes:nil
sgml-always-quote-attributes:t
sgml-indent-step:1
sgml-indent-data:t
sgml-parent-document:nil
sgml-default-dtd-file:"../reference.ced"
sgml-exposed-tags:nil
sgml-local-catalogs:"/usr/lib/sgml/catalog"
sgml-local-ecat-files:nil
End:
-->
