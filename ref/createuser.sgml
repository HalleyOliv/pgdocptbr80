<!--
$Header: /cvsroot/pgdocptbr/pgsgml800/doc/src/sgml/ref/createuser.sgml,v 1.11 2007/03/18 21:12:31 halleypo Exp $
PostgreSQL documentation
-->

<refentry id="APP-CREATEUSER">
 <refmeta>
  <refentrytitle id="APP-CREATEUSER-TITLE"><application>createuser</application></refentrytitle>
  <manvolnum>1</manvolnum>
  <refmiscinfo>Aplicativo</refmiscinfo>
 </refmeta>

 <refnamediv>
  <refname>createuser</refname>
  <refpurpose>cria uma conta de usu�rio do <productname>PostgreSQL</productname></refpurpose>
 </refnamediv>

 <indexterm zone="app-createuser">
  <primary>programa createuser</primary>
 </indexterm>

 <refsynopsisdiv>
  <cmdsynopsis>
   <command>createuser</command>
   <arg rep="repeat"><replaceable>op��o</replaceable></arg>
   <arg><replaceable>nome_do_usu�rio</replaceable></arg>
  </cmdsynopsis>
 </refsynopsisdiv>


 <refsect1>
  <title>Descri��o</title>

  <para>
   O utilit�rio <application>createuser</application> cria um novo usu�rio do
   <productname>PostgreSQL</productname>.
   Somente os superusu�rios (usu�rios com o campo
   <database class="table">usesuper</database> da tabela
   <database class="table">pg_shadow</database> definido como verdade) podem
   criar usu�rios do <productname>PostgreSQL</productname> e, portanto,
   o utilit�rio <application>createuser</application> deve ser executado por
   algu�m que possa se conectar como superusu�rio do <productname>PostgreSQL</>.
  </para>

  <para>
   Ser um superusu�rio tamb�m implica em n�o estar sujeito �s verifica��es
   de permiss�o de acesso do banco de dados e, portanto, a condi��o de
   superusu�rio deve ser concedida criteriosamente.
  </para>

  <para>
   O <application>createuser</application>  � um inv�lucro em torno do comando
   <xref linkend="SQL-CREATEUSER" endterm="SQL-CREATEUSER-title">
   do <acronym>SQL</acronym>.
   N�o existe diferen�a efetiva entre criar usu�rios atrav�s deste
   utilit�rio, ou atrav�s de outros m�todos para acessar o servidor.
  </para>

 </refsect1>


 <refsect1>
  <title>Op��es</title>

  <para>
   O <application>createuser</application> aceita os seguintes argumentos
   de linha de comando:

    <variablelist>
     <varlistentry>
      <term><replaceable class="parameter">nome_do_usu�rio</replaceable></term>
      <listitem>
       <para>
        Especifica o nome do usu�rio do <productname>PostgreSQL</productname>
        a ser criado. O nome deve ser �nico entre todos os usu�rios do
        <productname>PostgreSQL</productname>.
       </para>
      </listitem>
     </varlistentry>

     <varlistentry>
      <term><option>-a</></term>
      <term><option>--adduser</></term>
      <listitem>
       <para>
        Permite o novo usu�rio criar outros usu�rios (Nota: na verdade isto
        torna o novo usu�rio um <firstterm>superusu�rio</firstterm>.
        Esta op��o possui um nome equivocado).
       </para>
      </listitem>
     </varlistentry>

     <varlistentry>
      <term><option>-A</></term>
      <term><option>--no-adduser</></term>
      <listitem>
       <para>
        O novo usu�rio n�o pode criar outros usu�rios (ou seja,
        o novo usu�rio � um usu�rio normal, e n�o um superusu�rio).
        Este � o padr�o.
       </para>
      </listitem>
     </varlistentry>

     <varlistentry>
      <term><option>-d</></term>
      <term><option>--createdb</></term>
      <listitem>
       <para>
        O novo usu�rio tem permiss�o para criar bancos de dados.
       </para>
      </listitem>
     </varlistentry>

     <varlistentry>
      <term><option>-D</></term>
      <term><option>--no-createdb</></term>
      <listitem>
       <para>
        O novo usu�rio n�o tem permiss�o para criar bancos de dados.
        Este � o padr�o.
       </para>
      </listitem>
     </varlistentry>

     <varlistentry>
      <term><option>-e</></term>
      <term><option>--echo</></term>
      <listitem>
       <para>
        Mostra os comandos que o <application>createuser</application> gera
        e envia para o servidor.
       </para>
      </listitem>
     </varlistentry>

     <varlistentry>
      <term><option>-E</></term>
      <term><option>--encrypted</></term>
      <listitem>
       <para>
        Criptografa a senha do usu�rio armazenada no banco de dados. Se n�o for
        especificado, ser� utilizado o comportamento padr�o para senhas.
       </para>
      </listitem>
     </varlistentry>

     <varlistentry>
      <term><option>-i <replaceable class="parameter">n�mero</replaceable></></term>
      <term><option>--sysid <replaceable class="parameter">n�mero</replaceable></></term>
      <listitem>
       <para>
        Permite escolher uma identifica��o diferente do padr�o para o novo
        usu�rio. N�o � necess�rio, mas algumas pessoas gostam.
       </para>
      </listitem>
     </varlistentry>

     <varlistentry>
      <term><option>-N</></term>
      <term><option>--unencrypted</></term>
      <listitem>
       <para>
        N�o criptografa a senha do usu�rio armazenada no banco de dados. Se n�o
        for especificado, ser� utilizado o comportamento padr�o para senhas.
       </para>
      </listitem>
     </varlistentry>

     <varlistentry>
      <term><option>-P</></term>
      <term><option>--pwprompt</></term>
      <listitem>
       <para>
        Se for especificado, o <application>createuser</application>
        solicitar� a senha do novo usu�rio.
        N�o � necess�rio caso n�o se planeje usar autentica��o por senha.
       </para>
      </listitem>
     </varlistentry>

     <varlistentry>
      <term><option>-q</></term>
      <term><option>--quiet</></term>
      <listitem>
       <para>
        N�o exibe resposta.
       </para>
      </listitem>
     </varlistentry>
    </variablelist>
  </para>

  <para>
   Ser� solicitado o nome e outras informa��es que estejam faltando, se
   n�o forem especificadas na linha de comando.
  </para>

  <para>
   O <application>createuser</application> tamb�m aceita os seguintes
   argumentos de linha de comando para os par�metros de conex�o:

   <variablelist>
     <varlistentry>
      <term><option>-h <replaceable class="parameter">hospedeiro</replaceable></></term>
      <term><option>--host <replaceable class="parameter">hospedeiro</replaceable></></term>
      <listitem>
       <para>
        Especifica o nome de hospedeiro da m�quina onde o servidor est�
        executando. Se o nome iniciar por barra (/), ser� utilizado como
        o diret�rio do soquete do dom�nio <systemitem class="osname">Unix</>.
       </para>
      </listitem>
     </varlistentry>

     <varlistentry>
      <term><option>-p <replaceable class="parameter">porta</replaceable></></term>
      <term><option>--port <replaceable class="parameter">porta</replaceable></></term>
      <listitem>
       <para>
        Especifica a porta <acronym>TCP</acronym>, ou a extens�o do arquivo
        de soquete do dom�nio <systemitem class="osname">Unix</systemitem>
        local, onde o servidor est� atendendo as conex�es.
       </para>
      </listitem>
     </varlistentry>

     <varlistentry>
      <term><option>-U <replaceable class="parameter">nome_do_usu�rio</replaceable></></term>
      <term><option>--username <replaceable class="parameter">nome_do_usu�rio</replaceable></></term>
      <listitem>
       <para>
        Nome do usu�rio para conectar (e n�o o nome do usu�rio a ser criado).
       </para>
      </listitem>
     </varlistentry>

     <varlistentry>
      <term><option>-W</></term>
      <term><option>--password</></term>
      <listitem>
       <para>
        For�a a solicita��o da senha (para conectar ao servidor, e n�o
        a senha do novo usu�rio).
       </para>
      </listitem>
     </varlistentry>
   </variablelist>
  </para>
 </refsect1>


 <refsect1>
  <title>Ambiente</title>

  <variablelist>
   <varlistentry>
    <term><envar>PGHOST</envar></term>
    <term><envar>PGPORT</envar></term>
    <term><envar>PGUSER</envar></term>

    <listitem>
     <para>
      Par�metros de conex�o padr�o.
     </para>
    </listitem>
   </varlistentry>
  </variablelist>
 </refsect1>


 <refsect1>
  <title>Diagn�sticos</title>

  <para>
   Havendo dificuldade, veja no comando <xref linkend="SQL-CREATEUSER"
   endterm="sql-createuser-title"> e no <xref linkend="APP-PSQL">
   a discuss�o dos problemas poss�veis e as mensagens de erro.
   O servidor de banco de dados deve estar executando no
   hospedeiro de destino. Tamb�m se aplicam todas as defini��es de conex�o
   padr�o e as vari�veis de ambiente utilizadas pela biblioteca cliente
   <application>libpq</application>.
  </para>

 </refsect1>


 <refsect1>
  <title>Exemplos</title>

   <para>
    Para criar o usu�rio <systemitem class="username">joel</systemitem> no
    servidor de banco de dados padr�o:
<screen>
<prompt>$ </prompt><userinput>createuser joel</userinput>
<computeroutput>Is the new user allowed to create databases? (y/n) </computeroutput><userinput>n</userinput>
<computeroutput>Shall the new user be allowed to create more new users? (y/n) </computeroutput><userinput>n</userinput>
<computeroutput>CREATE USER</computeroutput>
</screen>
   </para>

   <para>
    Criar o mesmo usu�rio <systemitem class="username">joel</systemitem> usando
    o servidor no hospedeiro <systemitem class="systemname">eden</systemitem>,
    porta <systemitem class="resource">5000</systemitem>, evitando solicita��o
    de informa��o e vendo o comando subjacente:
<screen>
<prompt>$ </prompt><userinput>createuser -p 5000 -h eden -D -A -e joel</userinput>
<computeroutput>CREATE USER "joel" NOCREATEDB NOCREATEUSER</computeroutput>
<computeroutput>CREATE USER</computeroutput>
</screen>
   </para>
 </refsect1>


 <refsect1>
  <title>Consulte tamb�m</title>

  <simplelist type="inline">
   <member><xref linkend="app-dropuser"></member>
   <member><xref linkend="sql-createuser" endterm="sql-createuser-title"></member>
   <member>Vari�veis de ambiente (<xref linkend="libpq-envars">)</member>
  </simplelist>
 </refsect1>

</refentry>

<!-- Keep this comment at the end of the file
Local variables:
mode: sgml
sgml-omittag:nil
sgml-shorttag:t
sgml-minimize-attributes:nil
sgml-always-quote-attributes:t
sgml-indent-step:1
sgml-indent-data:t
sgml-parent-document:nil
sgml-default-dtd-file:"../reference.ced"
sgml-exposed-tags:nil
sgml-local-catalogs:"/usr/lib/sgml/catalog"
sgml-local-ecat-files:nil
End:
-->
