<!--
$Header: /cvsroot/pgdocptbr/pgsgml800/doc/src/sgml/ref/createdb.sgml,v 1.9 2007/03/18 21:12:31 halleypo Exp $
PostgreSQL documentation
-->

<refentry id="APP-CREATEDB">
 <refmeta>
  <refentrytitle id="APP-CREATEDB-TITLE"><application>createdb</application></refentrytitle>
  <manvolnum>1</manvolnum>
  <refmiscinfo>Aplicativo</refmiscinfo>
 </refmeta>

 <refnamediv>
  <refname>createdb</refname>
  <refpurpose>cria um banco de dados do <productname>PostgreSQL</productname></refpurpose>
 </refnamediv>

 <indexterm zone="app-createdb">
  <primary>programa createdb</primary>
 </indexterm>

 <refsynopsisdiv>
  <cmdsynopsis>
   <command>createdb</command>
   <arg rep="repeat"><replaceable>op��o</replaceable></arg>
   <arg><replaceable>nome_do_banco_de_dados</replaceable></arg>
   <arg><replaceable>descri��o</replaceable></arg>
  </cmdsynopsis>
 </refsynopsisdiv>


 <refsect1 id="R1-APP-CREATEDB-1">
  <title>
   Descri��o
  </title>
  <para>
   O <application>createdb</application> cria um banco de dados do
   <productname>PostgreSQL</productname>.
  </para>

  <para>
   Normalmente, o usu�rio do banco de dados que executa este comando se torna
   o dono do novo banco de dados. Entretanto, pode ser especificado um dono
   diferente por meio da op��o <option>-O</option>, se o usu�rio que est�
   executando este utilit�rio possuir os privil�gios apropriados.
  </para>

  <para>
   O <application>createdb</application> � um inv�lucro em torno do comando
   <xref linkend="SQL-CREATEDATABASE" endterm="SQL-CREATEDATABASE-title">
   do <acronym>SQL</acronym>.
   N�o existe diferen�a efetiva entre criar bancos de dados atrav�s deste
   utilit�rio, ou atrav�s de outros m�todos para acessar o servidor.
  </para>

 </refsect1>


 <refsect1>
  <title>Op��es</title>

  <para>
   O <application>createdb</application> aceita os seguintes argumentos de
   linha de comando:

    <variablelist>
     <varlistentry>
      <term><replaceable class="parameter">nome_do_banco_de_dados</replaceable></term>
      <listitem>
       <para>
        Especifica o nome do banco de dados a ser criado. O nome deve ser �nico
        entre todos os bancos de dados do <productname>PostgreSQL</productname>
        deste agrupamento. O padr�o � criar o banco de dados com o mesmo nome do
        usu�rio corrente do sistema operacional.
       </para>
      </listitem>
     </varlistentry>

     <varlistentry>
      <term><replaceable class="parameter">descri��o</replaceable></term>
      <listitem>
       <para>
        Especifica um coment�rio a ser associado
        ao banco de dados rec�m criado.
       </para>
      </listitem>
     </varlistentry>

     <varlistentry>
      <term><option>-D <replaceable class="parameter">espa�o_de_tabelas</replaceable></></term>
      <term><option>--tablespace <replaceable class="parameter">espa�o_de_tabelas</replaceable></></term>
      <listitem>
       <para>
        Especifica o espa�o de tabelas padr�o para o banco de dados.
       </para>
      </listitem>
     </varlistentry>

     <varlistentry>
      <term><option>-e</></term>
      <term><option>--echo</></term>
      <listitem>
       <para>
        Mostra os comandos que o <application>createdb</application>
        gera e envia para o servidor.
       </para>
      </listitem>
     </varlistentry>

     <varlistentry>
      <term><option>-E <replaceable class="parameter">codifica��o</replaceable></></term>
      <term><option>--encoding <replaceable class="parameter">codifica��o</replaceable></></term>
      <listitem>
       <para>
        Especifica o esquema de codifica��o de caracteres a ser usado neste
        banco de dados. Os conjuntos de caracteres suportados pelo servidor
        <productname>PostgreSQL</productname> est�o descritos na
        <xref linkend="multibyte-charset-supported">.
       </para>
      </listitem>
     </varlistentry>

     <varlistentry>
      <term><option>-O <replaceable class="parameter">dono</replaceable></></term>
      <term><option>--owner <replaceable class="parameter">dono</replaceable></></term>
      <listitem>
       <para>
        Especifica o usu�rio do banco de dados
        que ser� o dono do novo banco de dados.
       </para>
      </listitem>
     </varlistentry>

     <varlistentry>
      <term><option>-q</></term>
      <term><option>--quiet</></term>
      <listitem>
       <para>
        N�o exibe resposta.
       </para>
      </listitem>
     </varlistentry>

     <varlistentry>
      <term><option>-T <replaceable class="parameter">modelo</replaceable></></term>
      <term><option>--template <replaceable class="parameter">modelo</replaceable></></term>
      <listitem>
       <para>
        Especifica o banco de dados modelo,
        a partir do qual este banco de dados ser� constru�do.
       </para>
      </listitem>
     </varlistentry>
    </variablelist>
   </para>

   <para>
    As op��es <option>-D</option>, <option>-E</option>, <option>-O</option> e
    <option>-T</option> correspondem �s op��es do comando <acronym>SQL</>
    <xref linkend="SQL-CREATEDATABASE" endterm="SQL-CREATEDATABASE-title">
    subjacente; consulte este comando para obter informa��es adicionais
    sobre estas op��es.
   </para>

   <para>
    O <application>createdb</application> tamb�m aceita os seguintes
    argumentos de linha de comando para os par�metros de conex�o:

    <variablelist>
     <varlistentry>
      <term><option>-h <replaceable class="parameter">hospedeiro</replaceable></></term>
      <term><option>--host <replaceable class="parameter">hospedeiro</replaceable></></term>
      <listitem>
       <para>
        Especifica o nome de hospedeiro da m�quina onde o servidor est�
        executando.
        Se o nome iniciar por uma barra (/), ser� utilizado como o diret�rio do
        soquete do dom�nio <systemitem class="osname">Unix</systemitem>.
       </para>
      </listitem>
     </varlistentry>

     <varlistentry>
      <term><option>-p <replaceable class="parameter">porta</replaceable></></term>
      <term><option>--port <replaceable class="parameter">porta</replaceable></></term>
      <listitem>
       <para>
        Especifica a porta <acronym>TCP</acronym>, ou a extens�o de arquivo do
        soquete do dom�nio <systemitem class="osname">Unix</systemitem> local,
        onde o servidor est� atendendo as conex�es.
       </para>
      </listitem>
     </varlistentry>

     <varlistentry>
      <term><option>-U <replaceable class="parameter">nome_do_usu�rio</replaceable></></term>
      <term><option>--username <replaceable class="parameter">nome_do_usu�rio</replaceable></></term>
      <listitem>
       <para>
        Nome do usu�rio para conectar.
       </para>
      </listitem>
     </varlistentry>

     <varlistentry>
      <term><option>-W</></term>
      <term><option>--password</></term>
      <listitem>
       <para>
        For�a a solicita��o da senha.
       </para>
      </listitem>
     </varlistentry>
    </variablelist>
   </para>

 </refsect1>


 <refsect1>
  <title>Ambiente</title>

  <variablelist>
   <varlistentry>
    <term><envar>PGDATABASE</envar></term>
    <listitem>
     <para>
      Se estiver definida, o nome do banco de dados a ser criado, a menos que
      o nome esteja definido na linha de comando.
     </para>
    </listitem>
   </varlistentry>

   <varlistentry>
    <term><envar>PGHOST</envar></term>
    <term><envar>PGPORT</envar></term>
    <term><envar>PGUSER</envar></term>

    <listitem>
     <para>
      Par�metros de conex�o padr�o. <envar>PGUSER</envar> tamb�m
      determina o nome do banco de dados a ser criado, se este n�o for
      especificado na linha de comando ou por <envar>PGDATABASE</envar>.
     </para>
    </listitem>
   </varlistentry>
  </variablelist>
 </refsect1>


 <refsect1>
  <title>Diagn�sticos</title>

  <para>
   Havendo dificuldade, veja no comando <xref linkend="SQL-CREATEDATABASE"
   endterm="sql-createdatabase-title"> e no <xref linkend="APP-PSQL">
   a discuss�o dos problemas poss�veis e as mensagens de erro.
   O servidor de banco de dados deve estar executando no
   hospedeiro de destino. Tamb�m se aplicam todas as defini��es de conex�o
   padr�o e as vari�veis de ambiente utilizadas pela biblioteca cliente
   <application>libpq</application>.
  </para>

 </refsect1>


 <refsect1>
  <title>Exemplos</title>

   <para>
    Para criar o banco de dados <database class="name">demo</database>
    usando o servidor de banco de dados padr�o:
<screen>
<prompt>$ </prompt><userinput>createdb demo</userinput>
<computeroutput>CREATE DATABASE</computeroutput>
</screen>
    A resposta � a mesma que teria sido recebida se fosse executado o comando
    <command>CREATE DATABASE</command> do <acronym>SQL</acronym>.
   </para>

   <para>
    Para criar o banco de dados <database class="name">demo</database> usando o
    servidor no hospedeiro <systemitem class="systemname">eden</systemitem>,
    a porta <systemitem class="resource">5000</systemitem>, o esquema de
    codifica��o <systemitem>LATIN1</systemitem> e vendo o comando subjacente:
<screen>
<prompt>$ </prompt><userinput>createdb -p 5000 -h eden -E LATIN1 -e demo</userinput>
<computeroutput>CREATE DATABASE "demo" WITH ENCODING = 'LATIN1'</computeroutput>
<computeroutput>CREATE DATABASE</computeroutput>
</screen>
   </para>
 </refsect1>


 <refsect1>
  <title>Consulte tamb�m</title>

  <simplelist type="inline">
   <member><xref linkend="app-dropdb"></member>
   <member><xref linkend="sql-createdatabase" endterm="sql-createdatabase-title"></member>
   <member>Vari�veis de ambiente (<xref linkend="libpq-envars">)</member>
  </simplelist>
 </refsect1>

</refentry>

<!-- Keep this comment at the end of the file
Local variables:
mode: sgml
sgml-omittag:nil
sgml-shorttag:t
sgml-minimize-attributes:nil
sgml-always-quote-attributes:t
sgml-indent-step:1
sgml-indent-data:t
sgml-parent-document:nil
sgml-default-dtd-file:"../reference.ced"
sgml-exposed-tags:nil
sgml-local-catalogs:"/usr/lib/sgml/catalog"
sgml-local-ecat-files:nil
End:
-->
