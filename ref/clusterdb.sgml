<!--
$Header: /cvsroot/pgdocptbr/pgsgml800/doc/src/sgml/ref/clusterdb.sgml,v 1.11 2007/03/18 21:12:30 halleypo Exp $
PostgreSQL documentation
-->

<refentry id="APP-CLUSTERDB">
 <refmeta>
  <refentrytitle id="APP-CLUSTERDB-TITLE"><application>clusterdb</application></refentrytitle>
  <manvolnum>1</manvolnum>
  <refmiscinfo>Aplicativo</refmiscinfo>
 </refmeta>

 <refnamediv>
  <refname id="clusterdb">clusterdb</refname>
  <refpurpose>agrupa um banco de dados do <productname>PostgreSQL</productname></refpurpose>
 </refnamediv>

 <indexterm zone="app-clusterdb">
  <primary>programa clusterdb</primary>
 </indexterm>

 <refsynopsisdiv>
  <cmdsynopsis>
   <command>clusterdb</command>
   <arg rep="repeat"><replaceable>op��o_de_conex�o</replaceable></arg>
   <arg>--table | -t <replaceable>tabela</replaceable> </arg>
   <arg><replaceable>nome_do_banco_de_dados</replaceable></arg>
   <sbr>
   <command>clusterdb</command>
   <arg rep="repeat"><replaceable>op��o_de_conex�o</replaceable></arg>
   <group><arg>--all</arg><arg>-a</arg></group>
  </cmdsynopsis>
 </refsynopsisdiv>


 <refsect1>
  <title>Descri��o</title>

  <para>
   O <application>clusterdb</application> � um utilit�rio para reagrupar
   tabelas em um banco de dados do <productname>PostgreSQL</productname>.
   Encontra as tabelas que foram agrupadas anteriormente, e as agrupa
   novamente utilizando o mesmo �ndice usado da �ltima vez.
   As tabelas que nunca foram agrupadas n�o s�o afetadas.
  </para>

  <para>
   O <application>clusterdb</application> � um inv�lucro em torno do comando
   <xref linkend="SQL-CLUSTER" endterm="sql-cluster-title">
   do <acronym>SQL</acronym>.
   N�o existe diferen�a efetiva entre agrupar bancos de dado atrav�s
   deste utilit�rio, ou atrav�s de outros m�todos para acessar o servidor.
  </para>

 </refsect1>


 <refsect1>
  <title>Op��es</title>

   <para>
    O <application>clusterdb</application> aceita os seguintes argumentos
    de linha de comando:

    <variablelist>
     <varlistentry>
      <term><option>-a</></term>
      <term><option>--all</></term>
      <listitem>
       <para>
        Agrupa todos os bancos de dados.
       </para>
      </listitem>
     </varlistentry>

     <varlistentry>
      <term><option><optional>-d</> <replaceable class="parameter">nome_do_banco_de_dados</replaceable></></term>
      <term><option><optional>--dbname</> <replaceable class="parameter">nome_do_banco_de_dados</replaceable></></term>
      <listitem>
       <para>
        Especifica o nome do banco de dados a ser agrupado.
        Se n�o for especificado o nome do banco de dados, nem for utilizada a
        op��o <option>-a</option> (ou <option>--all</option>), o nome do banco
        de dados ser� obtido da vari�vel de ambiente <envar>PGDATABASE</envar>.
        Se esta	vari�vel n�o estiver definida, ent�o ser� utilizado o
        nome do usu�rio especificado na conex�o.
       </para>
      </listitem>
     </varlistentry>

     <varlistentry>
      <term><option>-e</></term>
      <term><option>--echo</></term>
      <listitem>
       <para>
        Mostra os comandos que o <application>clusterdb</application> gera
        e envia para o servidor.
       </para>
      </listitem>
     </varlistentry>

     <varlistentry>
      <term><option>-q</></term>
      <term><option>--quiet</></term>
      <listitem>
       <para>
        N�o exibe resposta.
       </para>
      </listitem>
     </varlistentry>

     <varlistentry>
      <term><option>-t <replaceable class="parameter">tabela</replaceable></></term>
      <term><option>--table <replaceable class="parameter">tabela</replaceable></></term>
      <listitem>
       <para>
        Agrupa somente a <replaceable class="parameter">tabela</replaceable>.
       </para>
      </listitem>
     </varlistentry>

    </variablelist>
   </para>

   <para>
    O <application>clusterdb</application> tamb�m aceita os seguintes
    argumentos de linha de comando para os par�metros de conex�o:

    <variablelist>
     <varlistentry>
      <term><option>-h <replaceable class="parameter">hospedeiro</replaceable></></term>
      <term><option>--host <replaceable class="parameter">hospedeiro</replaceable></></term>
      <listitem>
       <para>
        Especifica o nome de hospedeiro da m�quina onde o servidor est�
        executando. Se o nome iniciar por barra (/), ser� utilizado como
        o diret�rio do soquete do dom�nio <systemitem class="osname">Unix</>.
       </para>
      </listitem>
     </varlistentry>

     <varlistentry>
      <term><option>-p <replaceable class="parameter">porta</replaceable></></term>
      <term><option>--port <replaceable class="parameter">porta</replaceable></></term>
      <listitem>
       <para>
        Especifica a porta <acronym>TCP</acronym>, ou a extens�o do arquivo
        de soquete do dom�nio <systemitem class="osname">Unix</systemitem>
        local, onde o servidor est� atendendo as conex�es.
       </para>
      </listitem>
     </varlistentry>

     <varlistentry>
      <term><option>-U <replaceable class="parameter">nome_do_usu�rio</replaceable></></term>
      <term><option>--username <replaceable class="parameter">nome_do_usu�rio</replaceable></></term>
      <listitem>
       <para>
        Nome do usu�rio para conectar.
       </para>
      </listitem>
     </varlistentry>

     <varlistentry>
      <term><option>-W</></term>
      <term><option>--password</></term>
      <listitem>
       <para>
        For�a a solicita��o da senha.
       </para>
      </listitem>
     </varlistentry>
    </variablelist>
   </para>
 </refsect1>


 <refsect1>
  <title>Ambiente</title>

  <variablelist>
   <varlistentry>
    <term><envar>PGDATABASE</envar></term>
    <term><envar>PGHOST</envar></term>
    <term><envar>PGPORT</envar></term>
    <term><envar>PGUSER</envar></term>

    <listitem>
     <para>
      Par�metros de conex�o padr�o.
     </para>
    </listitem>
   </varlistentry>
  </variablelist>
 </refsect1>


 <refsect1>
  <title>Diagn�sticos</title>

  <para>
   Havendo dificuldade, veja no comando <xref linkend="SQL-CLUSTER"
   endterm="sql-cluster-title"> e no <xref linkend="APP-PSQL">
   a discuss�o dos problemas poss�veis e as mensagens de erro.
   O servidor de banco de dados deve estar executando no hospedeiro
   de destino. Tamb�m se aplicam todas as defini��es de conex�o padr�o
   e as vari�veis de ambiente utilizadas pela biblioteca cliente
   <application>libpq</application>.
  </para>

 </refsect1>


 <refsect1>
  <title>Exemplos</title>

   <para>
    Para agrupar o banco de dados <database class="name">teste</database>:
<screen>
<prompt>$ </prompt><userinput>clusterdb teste</userinput>
</screen>
   </para>

   <para>
    Para agrupar apenas a tabela <database class="table">foo</database>
    no banco de dados chamado <database class="name">xyzzy</database>:
<screen>
<prompt>$ </prompt><userinput>clusterdb --table foo xyzzy</userinput>
</screen>
   </para>

 </refsect1>

 <refsect1>
  <title>Consulte tamb�m</title>

  <simplelist type="inline">
   <member><xref linkend="sql-cluster" endterm="sql-cluster-title"></member>
   <member>Vari�veis de ambiente (<xref linkend="libpq-envars">)</member>
  </simplelist>
 </refsect1>

</refentry>

<!-- Keep this comment at the end of the file
Local variables:
mode: sgml
sgml-omittag:nil
sgml-shorttag:t
sgml-minimize-attributes:nil
sgml-always-quote-attributes:t
sgml-indent-step:1
sgml-indent-data:t
sgml-parent-document:nil
sgml-default-dtd-file:"../reference.ced"
sgml-exposed-tags:nil
sgml-local-catalogs:"/usr/lib/sgml/catalog"
sgml-local-ecat-files:nil
End:
-->
